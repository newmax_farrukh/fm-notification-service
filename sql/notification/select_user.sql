SELECT 
	n.*,

	-- User columns
	u.id "u.id",
	u.first_name "u.first_name",
	u.last_name "u.last_name",
	u.phone "u.phone",
	u.email "u.email",

	u.created_at "u.created_at",
	u.updated_at "u.updated_at",
	u.deleted_at "u.deleted_at",
	u.created_by "u.created_by",
	u.updated_by "u.updated_by",
	u.deleted_by "u.deleted_by",
	u.deleted "u.deleted"
FROM {table} n
INNER JOIN {user_table} u ON n.created_by=u.id
WHERE n.deleted=false AND n.viewed=false AND n.user_id=$1
ORDER BY $2
LIMIT $3
OFFSET $4
