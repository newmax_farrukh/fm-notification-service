package handlers

import (
	"github.com/k0kubun/pp"

	"notification/conf"
	"notification/db"
	"notification/models"

	"fm-libs/api"
	"fm-libs/config"
	"fm-libs/dbutil"
	"fm-libs/handler"
	"fm-libs/log"
	"fm-libs/util"
	"fm-libs/verr"

	"fmt"
	"os"
	"testing"
	"time"
)

var (
	s      = handler.BaseRpcReq{SessId: "1"}
	schema string
)

const (
	FID = "1d75255a-7362-4e3a-8c8d-7fdfe3eb687e"
	UID = "6aec3b6b-0f7c-4146-bbfc-729661c61f32"
)

func TestMain(m *testing.M) {
	var err error

	conf.ConfDir = "../conf"
	conf.RunMode = "test"

	cf, err := conf.GetConf()
	if err != nil {
		log.Log("get conf", log.FatalLevel, log.M{"error": err.Error()})
	}

	cf.SqlDir = "../sql"
	cf.MigrateDir = "../migrations"

	DB, err := db.GetDb()
	if err != nil {
		log.Log("get db", log.FatalLevel, log.M{"error": err.Error()})
	}

	verr.DB = DB

	if err := models.Init(); err != nil {
		log.Log("model init", log.FatalLevel, log.M{"error": err.Error()})
	}

	if err := Init(); err != nil {
		log.Log("handlers init", log.FatalLevel, log.M{"error": err.Error()})
	}

	if err := dbutil.ImportFile(DB.DB, cf.MigrateDir+"/test_data/handlers.sql"); err != nil {
		log.Log("handlers init", log.FatalLevel, log.M{"error": err.Error()})
	}

	schema = util.FleetSchema(FID)

	DB.MustExec("CREATE SCHEMA IF NOT EXISTS " + schema)

	fl := models.Fleet{}
	if err := fl.Create(FID); err != nil {
		log.Log("create fleet", log.FatalLevel, log.M{"error": err.Error()})
	}

	go Listen()

	time.Sleep(time.Second * 1)

	if err = api.InitClient(cf.Addr); err != nil {
		log.Log("init client", log.FatalLevel, log.M{"error": err.Error()})
	}

	handler.Session.Set("1", config.SessUserIdKey, UID)
	handler.Session.Set("1", config.SessFleetKey, FID)

	exit_code := m.Run()

	DB.MustExec("DROP SCHEMA IF EXISTS " + schema + " CASCADE")
	DB.MustExec("DELETE FROM user_users WHERE id='" + UID + "' ;")

	os.Exit(exit_code)
}

type Assert struct {
	Req  interface{}
	Code int
}

func Expect(method string, asserts []Assert) ([]api.Response, error) {
	resps := make([]api.Response, len(asserts))

	for _, a := range asserts {
		resp, err := api.CallRpcMethod(method, a.Req)
		if err != nil {
			return resps, err
		}
		if resp.ErrorCode != a.Code {
			err_msg := pp.Sprintln(fmt.Sprintf("%s expected: %d, got: %d", method, a.Code, resp.ErrorCode))
			err_msg += pp.Sprintln("req: ", a.Req)
			err_msg += pp.Sprintln("resp: ", resp)
			return resps, fmt.Errorf("%s", err_msg)
		}
		resps = append(resps, resp)
	}

	return resps, nil
}
