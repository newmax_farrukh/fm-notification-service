package handlers

import (
	"fm-libs/api"
	"testing"
)

var (
	WatchId string
)

func TestWatchCreate(t *testing.T) {
	asserts := []Assert{
		{WatchJson{BaseRpcReq: s, Entity: "", UserId: UID}, api.ErrCodeValidation},
		{WatchJson{BaseRpcReq: s, Entity: "issue", UserId: UID}, api.ErrCodeValidation},
		{WatchJson{
			BaseRpcReq: s,
			Entity:     "issue",
			UserId:     UID,
			Actions:    []string{"created", "updated"},
		}, api.ErrCodeSuccess},
	}

	resps, err := Expect("Watch.Create", asserts)
	if err != nil {
		t.Error(err)
		return
	}

	//get last success request
	//and get id from it
	resp := resps[len(resps)-1]

	resp_data, ok := resp.Data.(map[string]interface{})
	if !ok {
		t.Error("responce data convert to map interface error")
		return
	}

	id_interface, ok := resp_data["id"]
	if !ok {
		t.Error("resp_data id not found")
		return
	}

	WatchId, ok = id_interface.(string)
	if !ok {
		t.Error("id_interface convert to string error")
		return
	}
}

func TestWatchGet(t *testing.T) {
	asserts := []Assert{
		{WatchJson{BaseRpcReq: s, Id: "???"}, api.ErrCodeValidation},
		{WatchJson{BaseRpcReq: s, Id: ""}, api.ErrCodeValidation},
		{WatchJson{BaseRpcReq: s, Id: WatchId}, api.ErrCodeSuccess},
	}

	_, err := Expect("Watch.Get", asserts)
	if err != nil {
		t.Error(err)
		return
	}
}

func TestWatchUpdate(t *testing.T) {
	asserts := []Assert{
		{WatchJson{BaseRpcReq: s, Entity: "", UserId: UID}, api.ErrCodeValidation},
		{WatchJson{BaseRpcReq: s, Entity: "issue", UserId: UID}, api.ErrCodeValidation},
		{WatchJson{
			BaseRpcReq: s,
			Id:         WatchId,
			Entity:     "issue",
			UserId:     UID,
			Actions:    []string{"created", "updated"},
		}, api.ErrCodeSuccess},
	}

	_, err := Expect("Watch.Update", asserts)
	if err != nil {
		t.Error(err)
		return
	}
}

func TestWatchDelete(t *testing.T) {
	asserts := []Assert{
		{WatchJson{BaseRpcReq: s, Id: "???"}, api.ErrCodeValidation},
		{WatchJson{BaseRpcReq: s, Id: ""}, api.ErrCodeValidation},
		{WatchJson{BaseRpcReq: s, Id: WatchId}, api.ErrCodeSuccess},
	}

	_, err := Expect("Watch.Delete", asserts)
	if err != nil {
		t.Error(err)
		return
	}
}

func TestWatchRestore(t *testing.T) {
	asserts := []Assert{
		{WatchJson{BaseRpcReq: s, Id: "???"}, api.ErrCodeValidation},
		{WatchJson{BaseRpcReq: s, Id: ""}, api.ErrCodeValidation},
		{WatchJson{BaseRpcReq: s, Id: WatchId}, api.ErrCodeSuccess},
	}

	_, err := Expect("Watch.Restore", asserts)
	if err != nil {
		t.Error(err)
		return
	}
}

func TestWatchGetByUser(t *testing.T) {
	asserts := []Assert{
		{WatchJson{BaseRpcReq: s}, api.ErrCodeSuccess},
	}

	_, err := Expect("Watch.GetByUser", asserts)
	if err != nil {
		t.Error(err)
		return
	}
}
