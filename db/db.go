package db

import (
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"

	"notification/conf"

	"fmt"
)

var (
	db *sqlx.DB      //db instance
	cf *conf.AppConf //config
)

func initConf() error {
	//for not creating config in every call
	if cf != nil {
		return nil
	}

	var err error
	cf, err = conf.GetConf()

	if err != nil {
		fmt.Errorf("get config error: ", err)
	}

	return nil
}

func GetDb() (*sqlx.DB, error) {
	if err := initConf(); err != nil {
		return nil, err
	}

	//return db instance if initialized
	if db != nil {
		return db, nil
	}

	var err error
	db, err = sqlx.Connect(cf.Db.Driver, cf.Db.Dsn())

	if err != nil {
		return nil, fmt.Errorf(
			"db connect error: %s, dsn: %s",
			err,
			cf.Db.Dsn(),
		)
	}

	return db, nil
}
