package models

import (
	"fm-libs/dbutil"
	"fm-libs/util"

	"notification/conf"
	"notification/db"

	"log"
	"os"
	"testing"
)

var (
	FID        = "afbadde1-e698-4bea-a328-4819f4a44e45"
	UID        = "754bbb75-b31d-4c2b-b1df-035f248d9e8e"
	VEHICLE_ID = "aece4ba1-cc98-43e7-9773-ab462d6e2953"

	schema string
)

func TestMain(m *testing.M) {
	conf.ConfDir = "../conf"
	conf.RunMode = "test"

	cf, err := conf.GetConf()
	if err != nil {
		log.Fatal("get conf error: ", err)
	}

	cf.MigrateDir = "../migrations"
	cf.SqlDir = "../sql"

	DB, err := db.GetDb()
	if err != nil {
		log.Fatal("get db error: ", err)
	}

	err = Init()
	if err != nil {
		log.Fatal(err)
	}

	if err = dbutil.ImportFile(DB.DB, cf.MigrateDir+"/test_data/models.sql"); err != nil {
		log.Fatal("test data insert error: ", err)
	}

	schema = util.FleetSchema(FID)

	DB.MustExec("CREATE SCHEMA IF NOT EXISTS " + schema)

	fl := Fleet{}
	if err := fl.Create(FID); err != nil {
		log.Fatal("fleet create erorr: ", err)
	}

	exit_code := m.Run()

	DB.MustExec("DROP SCHEMA IF EXISTS " + schema + " CASCADE;")
	DB.MustExec("DELETE FROM user_users WHERE id='" + UID + "' ;")

	os.Exit(exit_code)
}
