package models

import (
	"fm-libs/dbutil"
	"fm-libs/orm"
	"fm-libs/util"

	"strings"
)

type User struct {
	Id        string `db:"id" json:"id"`
	Email     string `db:"email" json:"email"`
	FirstName string `db:"first_name" json:"first_name"`
	LastName  string `db:"last_name" json:"last_name"`
	Phone     string `db:"phone" json:"phone"`

	orm.Std
}

type Notification struct {
	Id      string `db:"id" json:"id"`
	Content string `db:"content" json:"content"`
	Viewed  bool   `db:"viewed" json:"viewed"`
	UserId  string `db:"user_id" json:"user_id"`
	FleetId string `db:"fleet_id" json:"fleet_id"`
	Entity  string `db:"entity" json:"entity"`
	Action  string `db:"action" json:"action"`

	//joins
	Creator User `db:"u" json:"creator"`

	orm.Std
}

func (m Notification) GetName() string {
	return "notification"
}

func (m Notification) GetTable() string {
	return "ntf_notifications"
}

func (m *Notification) SetId(id string) {
	m.Id = id
}

//Orm helpers. Making models acting like active record
func (m *Notification) Create() error {
	args := orm.Args{"table": m.GetTable()}
	return ORM.Insert(m, args, true)
}

func (m *Notification) Update(ntf_ids []string) error {

	for i, ntf_id := range ntf_ids {
		ntf_ids[i] = "'" + ntf_id + "'"
	}

	args := orm.Args{"table": m.GetTable(), "notification_ids": strings.Join(ntf_ids, ",")}
	return ORM.Update(m, args)
}

func (m *Notification) Get(id string) error {
	return ORM.Get(m, orm.ModelSqlFile(m, "getbypk"), orm.Args{
		"table": m.GetTable(),
	}, id)
}

func (m *Notification) GetByUser(user_id string, sort []string, page, pageSize int) ([]Notification, int, error) {
	dests := []Notification{}

	order := dbutil.OrderMap(map[string]string{
		"id": "id",
	}, sort, "id ASC")

	cnt, err := m.CountByUser(user_id)
	if err != nil {
		return nil, 0, err
	}

	offset, pages := util.GetPageOffset(page, pageSize, cnt)

	if offset == -1 {
		return nil, 0, ErrNotFound
	}

	if pages == -1 {
		return nil, 0, ErrPageNotFound
	}

	if pageSize < 1 {
		pageSize = 10
	}

	err = ORM.Select(&dests, orm.ModelSqlFile(m, "select_user"), orm.Args{
		"table":      m.GetTable(),
		"user_table": "user_users",
	}, user_id, order, pageSize, offset)

	if err != nil {
		return nil, 0, err
	}

	return dests, pages, nil
}

func (m *Notification) Delete() error {
	args := orm.Args{"table": m.GetTable()}
	return ORM.Delete(m, args)
}

func (m *Notification) Restore() error {
	args := orm.Args{"table": m.GetTable()}
	return ORM.Restore(m, args)
}

func (m *Notification) CountByUser(user_id string) (int, error) {
	var count int
	args := orm.Args{
		"table":      m.GetTable(),
		"user_table": "user_users",
	}
	err := ORM.Get(&count, orm.ModelSqlFile(m, "count_user"), args, user_id)

	if err != nil {
		return 0, err
	}

	return count, nil
}
