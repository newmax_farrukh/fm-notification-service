package models

import (
	"fm-libs/orm"
	"fm-libs/util"
)

type Fleet struct {
}

//Create new tables in a new fleet schema
func (m *Fleet) Create(fid string) error {
	schema := util.FleetSchema(fid)
	args := orm.Args{
		"schema":      schema,
		"watch_table": "ntf_watches",
	}

	_, err := ORM.Exec("fleet/create", args)
	return err
}
