package models

import (
	"fm-libs/orm"
	"testing"
)

var (
	NotificationId     string
	NotificationUserId = "aece4ba1-cc98-43e7-9773-ab462d6e2953"
)

func TestNotificationCreate(t *testing.T) {
	model := Notification{}
	model.UserId = NotificationUserId
	model.Content = `{"field": "value"}`
	model.FleetId = FID
	model.Entity = "issue"
	model.Action = "created"
	model.CreatedBy = UID

	t.Logf("model: %#v \n", model)
	if err := model.Create(); err != nil {
		t.Error(err)
	}

	NotificationId = model.Id
}

func TestNotificationGetByUser(t *testing.T) {
	model := Notification{}
	dests, pages, err := model.GetByUser(NotificationUserId, []string{""}, 0, 0)
	if err != nil {
		t.Error(err)
	}
	if len(dests) < 1 {
		t.Error("dests length should be grater than 0")
		return
	}

	if pages < 1 {
		t.Error("pages should be grater than 0")
		return
	}

	if dests[0].Creator.Id != UID {
		t.Errorf("model.Creator.Id expected: %s, got: %s", UID, dests[0].Creator.Id)
	}
}

func TestNotificationUpdate(t *testing.T) {
	model := Notification{}
	model.Viewed = true
	model.UpdatedBy.String = UID
	model.UpdatedBy.Valid = true

	if err := model.Update([]string{NotificationId}); err != nil {
		t.Error("update error: ", err)
	}
}

func TestNotificationGet(t *testing.T) {
	model := Notification{}
	if err := model.Get(NotificationId); err != nil {
		t.Errorf("model: %+v ,\n err: %v", model, err)
		return
	}

	t.Logf("model: %#v \n", model)

	if model.UserId != NotificationUserId {
		t.Errorf("expected UserId: %s, got: %s \n", NotificationUserId, model.UserId)
	}

}

func TestNotificationDelete(t *testing.T) {
	model := Notification{}
	model.Id = NotificationId
	model.DeletedBy.String = UID
	model.DeletedBy.Valid = true

	if err := model.Delete(); err != nil {
		t.Error(err)
	}
}

func TestNotificationRestore(t *testing.T) {
	model := Notification{}
	model.Id = NotificationId
	if err := model.Restore(); err != nil {
		t.Error("ls", err)
	}

	//@TODO cleaning test data after tests
	//if you add new test move it to last text function
	_, err := ORM.Exec("test/notification_delete", orm.Args{"table": model.GetTable()}, NotificationId)
	if err != nil {
		t.Error("test data delete error: ", err)
	}
}
